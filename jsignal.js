const { customAlphabet } = require('nanoid');
const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
const nanoid = customAlphabet(alphabet, 7);

module.exports = (() => {

	let sessions = {};

	/*
		sessions = {}
			[key]: {
				key: UUID,
				sender: SocketConnection,
				lastAliveTime: timeMillis,
				
				transmissions: {
					[id]: {
						id: UUID,
						receiver: SocketConnection,
						lastAliveTime: timeMillis
					},
					...
				}
			},
			...
		}


		Workflow:

		- Sender creates a session (receives a key).
		- Receiver enters session (by entering the key) (receives transmissionID)
		- Sender is notified about new transmission, creates new connection endpoint (RTCPeerConnection), creates offer, send offer to this API
		- Receiver is notified about offer, creates a new connection endpoint, creates an answer, send answer to this API
		- Sender is notified about transmission answer, finalizes endpoint setup, ice begins


		If a receiver looses connection, he can try to revive the connection, or just create a completely new transmission

		Sender and receiver keep sending "keepAlive" to this API
		If sender detects a transmission is not alive anymore, he can close the connection
		If receiver detects the sender is not alive anymore, he can close the connection
		If this system detecs that neither the sender nor any transmission is alive anymore, delete the session
		
	*/

	function createSession(senderSocketConnection, wantedKey){
		
		let key

		if(wantedKey === 'Blau'){
			key = wantedKey
		} else {
			key = nanoid();
		}

		sessions[key] = {
			key: key,
			sender: senderSocketConnection,
			lastAliveTime: new Date(),
			transmissions: {}
		};

		return key;
	}

	function getSession(sessionKey){
		return sessions[sessionKey];
	}

	function createTransmission(sessionKey, receiverSocketConnection){
		let session = sessions[sessionKey];
		if(!session){
			throw new Error('session not found');
		}

		let id = nanoid();

		session.transmissions[id] = {
			id: id,
			receiver: receiverSocketConnection,
			lastAliveTime: new Date()
		}

		return id;
	}

	function getTransmission(sessionKey, transmissionId){
		let session = sessions[sessionKey];
		if(!session){
			return undefined;
		}

		return session.transmissions[transmissionId];
	}

	function updateAliveSession(sessionKey){
		if(sessions[sessionKey]){
			sessions[sessionKey].lastAliveTime = new Date();	
		}
	}

	function updateAliveTransmission(sessionKey, transmissionId){
		if(sessions[sessionKey] && sessions[sessionKey].transmissions[transmissionId]){
			sessions[sessionKey].transmissions[transmissionId].lastAliveTime = new Date();	
		}
	}

	return {
		createSession: createSession,
		getSession: getSession,
		createTransmission: createTransmission,
		getTransmission: getTransmission,
		updateAliveSession: updateAliveSession,
		updateAliveTransmission: updateAliveTransmission
	}
})()
