let WebSocketServer = require('websocket').server;
let http = require('http');

let jsignal = require('./jsignal.js');

let server = http.createServer(function(request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});

const PORT = 3750

server.listen(PORT, function() {
    console.log((new Date()) + ' Server is listening on port ' + PORT);
});

wsServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});

function originIsAllowed(origin) {
	// TODO
  // put logic here to detect whether the specified origin is allowed.
  return true;
}

wsServer.on('request', function(request) {
    if (!originIsAllowed(request.origin)) {
      // Make sure we only accept requests from an allowed origin
      request.reject();
      console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
      return;
    }
    
    let connection = request.accept('javis-protocol', request.origin);
    
    console.log((new Date()) + ' ' + connection.socket.remoteAddress + ':' + connection.socket.remotePort + ' Connection accepted');

    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            console.log('\nReceived Message (' + connection.socket.remoteAddress + ':' + connection.socket.remotePort +'): ' + message.utf8Data + '\n');
            
            handleMessage(connection, message.utf8Data)
        }
        else if (message.type === 'binary') {
            console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
            //connection.sendBytes(message.binaryData);
        }
    });

    connection.on('close', function(reasonCode, description) {
        console.log((new Date()) + ' Peer ' + connection.socket.remoteAddress + ' disconnected.');
    });
});


function handleMessage(connection, message){
	try {
		let msg = JSON.parse(message);

		switch(msg.type){
			case 'create-session' : {
				let sessionKey = jsignal.createSession(connection, msg.content.wantedKey)
			    sendMessage(connection, 'session-key', sessionKey);
			}; break;

			case 'create-transmission' : {
				let session = jsignal.getSession(msg.content.sessionKey)
				if(!session){
					sendMessage(connection, 'error', 'invalid session key');
					return
				}

				let transmissionId = jsignal.createTransmission(msg.content.sessionKey, connection);
				sendMessage(connection, 'transmission-id', transmissionId);

				sendMessage(session.sender, 'new-transmission', {
					transmissionId: transmissionId
				});
			}; break;

			case 'transmission-decline' : {
				let session = jsignal.getSession(msg.content.sessionKey)
				if(!session){
					sendMessage(connection, 'error', 'invalid session key');
					return
				}

				if(!sameConnection(connection, session.sender)){
					// not allowed
					console.log('"transmission-decline" not allowed: bad session key', connection.socket.remotePort);
					connection.close(1008, 'bad session key');
					return;
				}

				if(!msg.content.transmissionId){
					sendMessage(connection, 'error', 'invalid transmissionId');
					return
				}

				let transmission = jsignal.getTransmission(msg.content.sessionKey, msg.content.transmissionId)
				if(!transmission){
					sendMessage(connection, 'error', 'invalid transmissionId');
					return
				}
				
				sendMessage(transmission.receiver, 'transmission-decline', {
					transmissionId: msg.content.transmissionId
				});
			}; break;

			case 'transmission-offer' : {
				let session = jsignal.getSession(msg.content.sessionKey)
				if(!session){
					sendMessage(connection, 'error', 'invalid session key');
					return
				}

				if(!sameConnection(connection, session.sender)){
					// not allowed
					console.log('"transmission-offer" not allowed: bad session key', connection.socket.remotePort);
					connection.close(1008, 'bad session key');
					return;
				}

				if(!msg.content.transmissionId){
					sendMessage(connection, 'error', 'invalid transmissionId');
					return
				}

				if(!msg.content.offerDescription){
					sendMessage(connection, 'error', 'invalid offerDescription');
					return
				}

				let transmission = jsignal.getTransmission(msg.content.sessionKey, msg.content.transmissionId)
				if(!transmission){
					sendMessage(connection, 'error', 'invalid transmissionId');
					return
				}
				
				sendMessage(transmission.receiver, 'transmission-offer', {
					transmissionId: msg.content.transmissionId,
					offerDescription: msg.content.offerDescription
				});
			}; break;

			case 'transmission-answer' : {
				let session = jsignal.getSession(msg.content.sessionKey)
				if(!session){
					sendMessage(connection, 'error', 'invalid session key');
					return
				}

				if(!msg.content.transmissionId){
					sendMessage(connection, 'error', 'invalid transmissionId');
					return
				}

				let transmission = jsignal.getTransmission(msg.content.sessionKey, msg.content.transmissionId)
				if(!transmission){
					sendMessage(connection, 'error', 'invalid transmissionId');
					return
				}

				if(!sameConnection(connection, transmission.receiver)){
					// not allowed
					console.log('"transmission-answer" not allowed: bad transmissionId key', connection.socket.remotePort);
					connection.close(1008, 'bad transmissionId key');
					return;
				}

				if(!msg.content.answerDescription){
					sendMessage(connection, 'error', 'invalid answerDescription');
					return
				}

				sendMessage(session.sender, 'transmission-answer', {
					transmissionId: msg.content.transmissionId,
					answerDescription: msg.content.answerDescription
				})
			}; break;

			case 'ice-candidate' : {
				let session = jsignal.getSession(msg.content.sessionKey)
				if(!session){
					sendMessage(connection, 'error', 'invalid session key');
					return
				}

				if(!msg.content.transmissionId){
					sendMessage(connection, 'error', 'invalid transmissionId');
					return
				}

				if(!msg.content.candidate){
					sendMessage(connection, 'error', 'invalid candidate');
					return
				}

				let transmission = jsignal.getTransmission(msg.content.sessionKey, msg.content.transmissionId)

				if(sameConnection(connection, session.sender)){
					// send data to receiver					
					if(!transmission){
						sendMessage(connection, 'error', 'invalid transmissionId');
						return
					}
					//TODO check if receiver is actually set, if not, delay this
					sendMessage(transmission.receiver, 'ice-candidate', {
						transmissionId: msg.content.transmissionId,
						candidate: msg.content.candidate
					});
				} else if (sameConnection(connection, transmission.receiver)) {
					// send data to sender
					sendMessage(session.sender, 'ice-candidate', {
						transmissionId: msg.content.transmissionId,
						candidate: msg.content.candidate
					});
				} else {
					// not allowed
					console.log('"ice-candidate" not allowed: bad transmissionId', connection.socket.remotePort);
					connection.close(1008, 'bad transmissionId key');
					return;
				}
			}; break;

			case 'keep-alive' : {
				let session = jsignal.getSession(msg.content.sessionKey);
				if(!session){
					sendMessage(connection, 'error', 'invalid session key');
					return
				}

				if(sameConnection(connection, session.sender)){
					jsignal.updateAliveSession(msg.content.sessionKey);
				} else {
					if(!msg.content.transmissionId){
						sendMessage(connection, 'error', 'invalid transmissionId');
						return
					}

					let transmission = jsignal.getTransmission(msg.content.sessionKey, msg.content.transmissionId)

					if (sameConnection(connection, transmission.receiver)){
						jsignal.updateAliveTransmission(msg.content.sessionKey, msg.content.transmissionId);
					} else {
						// not allowed
						console.log('"keep-alive" not allowed: bad transmissionId key', connection.socket.remotePort);
						connection.close(1008, 'bad transmissionId key');
						return;
					}
				}
			}; break;

			default : {
				console.log('dropped invalid message: unsupported type');
			}
		}
	} catch (ex){
		console.log('dropped invalid message: unparsable', ex);
	}
}


function sendMessage(connection, type, data){
	let json = JSON.stringify({
		type: type,
		content: data
	})
	connection.sendUTF(json);
}

//TODO improve
function sameConnection(conA, conB){
	return conA && conB && conA.socket && conB.socket && conA.socket.remoteAddress === conB.socket.remoteAddress && conA.socket.remotePort === conB.socket.remotePort;
}

/*

var express = require('express')
var path = require('path')
var app = express()

app.disable('x-powered-by');

var helmet = require('helmet');
app.use(helmet());

var hsts = require('hsts')
app.use(hsts({
  maxAge: 15552000  // 180 days in seconds
}))

var jsignal = require('./jsignal.js')

var handlebars = require('express-handlebars') // renderer for express
// Register '.handlebars' extension with Handlebars
app.engine('handlebars', handlebars({defaultLayout: 'default'}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'handlebars');

/*app.use((req, res, next)=>{
	console.log('got new request')
	next()
})


app.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.header("Access-Control-Allow-Methods", "DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range");
        res.header("Access-Control-Expose-Headers", "Content-Length,Content-Range");
        console.log('added headers')
        next();
});


var logger = require('morgan')
app.use(logger('dev'))

var bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))



app.get('/', function(req, res, next) {   
    res.render('index');
});

/*
app.get('/api/get-offers', async (req, res)=>{
	if(req.query.auth === '1amIMPORTANT_!!!'){
		var offers = await avis.getOffers()
		var response = {
				status: 'success',
				offer_length: offers.length,
				offers: offers,
			}
		if(req.query.pretty){
			res.send('<textarea rows="30" cols="100">'+JSON.stringify(response, null, 2)+'</textarea>')
		} else {
			res.json(response)
		}
	} else {
		res_error(res, 'you can only post offers')		
	}
})

app.get('/api/get-registrar-desc', async (req, res)=>{
	avis.getOfferRegistrarDesc(req.query.key).then((desc)=>{
		res.json({
			status: 'success',
			description: desc
		})
	}).catch((err)=>{
		res_error(res, 'offer with that key not found')
	})
})

app.post('/api/offer', async (req, res)=>{
	avis.offer(req.connection.remoteAddress, req.body.description).then((key)=>{
		res.json({
			status: 'success',
			key: key
		})
	}).catch((e)=>{
		res_error(res, e)
	})

})

app.options('/api/offer', async (req, res)=>{
	res.header('Allow', 'POST')
	res.send('')
})

app.post('/api/answer', (req, res)=>{
	avis.answer(req.connection.remoteAddress, req.body.key, req.body.description).then((description)=>{
		res.json({
			status: 'success',
			description: description
		})
	}).catch((e)=>{
		res_error(res, e)
	})
})

app.options('/api/answer', async (req, res)=>{
	res.header('Allow', 'POST')
	res.send('')
})

app.get('/api/get-answer', (req, res)=>{
	avis.getAnswer(req.query.key).then((description)=>{
		res.json({
			status: 'success',
			description: description
		})
	}).catch((e)=>{
		res_error(res, e)
	})
})

app.post('/api/candidate', (req, res)=>{
	avis.addIceCandidate(req.connection.remoteAddress, req.body.key, req.body.candidate, req.body.my_type).then(()=>{
		res.json({
			status: 'success'
		})
	}).catch((e)=>{
		res_error(res, e)
	})
})

app.options('/api/candidate', async (req, res)=>{
	res.header('Allow', 'POST')
	res.send('')
})

app.get('/api/get-candidate', (req, res)=>{
	avis.getIceCandidate(req.connection.remoteAddress, req.query.key, req.query.my_type).then((candidate)=>{
		res.json({
			status: 'success',
			candidate: candidate
		})
	}).catch((e)=>{
		res_error(res, e)
	})
})

function res_error(res, message){
	res.json({
		status: 'error',
		error: message
	})
}






// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

*/